package com.adp.interview.model;
/**
 * 
 * @author sudhanshu
 *
 */
public class CommonAttribute {

	private String name;
	private String displayName;
	private String type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
