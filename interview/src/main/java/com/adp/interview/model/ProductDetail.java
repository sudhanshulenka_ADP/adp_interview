package com.adp.interview.model;

import java.math.BigDecimal;

/**
 * 
 * @author sudhanshu
 *
 */
public class ProductDetail extends CommonAttribute {

	private String productId;
	private CategoryDetail category;
	private Double avilableQuantity;
	private String unit;
	private BigDecimal price;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public CategoryDetail getCategory() {
		return category;
	}

	public void setCategory(CategoryDetail category) {
		this.category = category;
	}

	public Double getAvilableQuantity() {
		return avilableQuantity;
	}

	public void setAvilableQuantity(Double avilableQuantity) {
		this.avilableQuantity = avilableQuantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
