package com.adp.interview.model;

import java.math.BigDecimal;
import java.util.Date;

public class BillProduct {

	private String productId;
	private String productBillId;
	private Double orderQuantity;
	private BigDecimal totalPrice=BigDecimal.ZERO;
	private BigDecimal levyPrice=BigDecimal.ZERO;;
	private BigDecimal taxPrice=BigDecimal.ZERO;;
	private Date purchaseDate;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductBillId() {
		return productBillId;
	}

	public void setProductBillId(String productBillId) {
		this.productBillId = productBillId;
	}

	public Double getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(Double orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getLevyPrice() {
		return levyPrice;
	}

	public void setLevyPrice(BigDecimal levyPrice) {
		this.levyPrice = levyPrice;
	}

	public BigDecimal getTaxPrice() {
		return taxPrice;
	}

	public void setTaxPrice(BigDecimal taxPrice) {
		this.taxPrice = taxPrice;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

}
