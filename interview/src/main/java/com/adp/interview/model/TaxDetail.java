package com.adp.interview.model;

import java.math.BigDecimal;
/**
 * 
 * @author sudhanshu
 *
 */
public class TaxDetail extends CommonAttribute {

	private String taxId;
	private BigDecimal value;

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
