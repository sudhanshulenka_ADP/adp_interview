package com.adp.interview.model;
/**
 * 
 * @author sudhanshu
 *
 */
public class CategoryDetail extends CommonAttribute {

	private String categoryId;
	private boolean isLevyApplied;
	private LevyDetail appliedLevy;
	private TaxDetail appliedTaxType;

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public boolean isLevyApplied() {
		return isLevyApplied;
	}

	public void setLevyApplied(boolean isLevyApplied) {
		this.isLevyApplied = isLevyApplied;
	}

	public LevyDetail getAppliedLevy() {
		return appliedLevy;
	}

	public void setAppliedLevy(LevyDetail appliedLevy) {
		this.appliedLevy = appliedLevy;
	}

	public TaxDetail getAppliedTaxType() {
		return appliedTaxType;
	}

	public void setAppliedTaxType(TaxDetail appliedTaxType) {
		this.appliedTaxType = appliedTaxType;
	}

}
