package com.adp.interview.model;

import java.math.BigDecimal;

import com.adp.interview.constant.LevyStatus;

/**
 * 
 * @author sudhanshu
 *
 */
public class LevyDetail extends CommonAttribute {
	private String levyId;
	private BigDecimal amount;
	private String status;

	public String getLevyId() {
		return levyId;
	}

	public void setLevyId(String levyId) {
		this.levyId = levyId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
