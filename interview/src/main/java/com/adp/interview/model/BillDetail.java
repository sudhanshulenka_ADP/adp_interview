package com.adp.interview.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @author sudhanshu
 *
 */
public class BillDetail extends CommonAttribute {

	private String billId;
	private List<BillProduct> productList;
	private BigDecimal billedAmount = BigDecimal.ZERO;;
	private BigDecimal totalLevyAmount = BigDecimal.ZERO;;
	private BigDecimal totalTaxAmount = BigDecimal.ZERO;;

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public List<BillProduct> getProductList() {
		return productList;
	}

	public void setProductList(List<BillProduct> productList) {
		this.productList = productList;
	}

	public BigDecimal getBilledAmount() {
		return billedAmount;
	}

	public void setBilledAmount(BigDecimal billedAmount) {
		this.billedAmount = billedAmount;
	}

	public BigDecimal getTotalLevyAmount() {
		return totalLevyAmount;
	}

	public void setTotalLevyAmount(BigDecimal totalLevyAmount) {
		this.totalLevyAmount = totalLevyAmount;
	}

	public BigDecimal getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BillDetail [billId=" + billId + ", productList=" + productList + ", billedAmount=" + billedAmount
				+ ", totalLevyAmount=" + totalLevyAmount + ", totalTaxAmount=" + totalTaxAmount + "]";
	}

}
