package com.adp.interview.constant;

/**
 * 
 * @author sudhanshu
 *
 */
public enum PaymentStatus {
	PAID, UN_PAID, GATEWAY_ISSUE, INITIATE_PAYEMNT, FAILED, GENERATED, SUCCEEDED
}
