package com.adp.interview.constant;

/**
 * 
 * @author sudhanshu
 *
 */
public enum LevyStatus {
	ACTIVE, IN_ACTIVE, EXPIRED
}
