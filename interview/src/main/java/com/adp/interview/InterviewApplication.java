package com.adp.interview;

import java.util.Scanner;

import com.adp.interview.model.BillDetail;
import com.adp.interview.model.BillProduct;
import com.adp.interview.service.BillDetailService;
import com.adp.interview.service.impl.BillDetailServiceImpl;

public class InterviewApplication {

	public static void main(String[] args) throws Exception {
		InterviewApplication ia = new InterviewApplication();
		ia.scannProductForBill();
	}

	public void scannProductForBill() throws Exception {
		System.out.println("Please enter product id to be scann : ");
		Scanner scanner = new Scanner(System.in);
		String productId = scanner.nextLine();
		System.out.println("Enter Quantity for this product : " + productId);
		int quantity = scanner.nextInt();
		BillDetailService billDetailService = new BillDetailServiceImpl();
		BillDetail bill = new BillDetail();
		BillProduct billProduct = new BillProduct();
		billProduct.setProductId(productId);
		billProduct.setOrderQuantity(Double.valueOf(quantity));
		billDetailService.processBillForProduct(billProduct);
		billDetailService.checkOutBill(bill, billProduct);
		System.out.println("Your Bill : " + bill);
	}

}
