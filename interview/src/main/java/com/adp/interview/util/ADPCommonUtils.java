package com.adp.interview.util;

import org.apache.commons.lang3.RandomStringUtils;

public class ADPCommonUtils {

	public static String generateProductId() {
		return RandomStringUtils.randomAlphabetic(6);
	}

	public static String generateCategoryId() {
		return RandomStringUtils.randomAlphabetic(4);
	}

	public static String generateLevyId() {
		return RandomStringUtils.randomAlphabetic(3);
	}

	public static String generateTaxId() {
		return RandomStringUtils.randomAlphabetic(3);
	}

}
