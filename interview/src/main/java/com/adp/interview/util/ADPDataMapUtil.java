package com.adp.interview.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.adp.interview.model.BillDetail;
import com.adp.interview.model.CategoryDetail;
import com.adp.interview.model.CommonAttribute;
import com.adp.interview.model.LevyDetail;
import com.adp.interview.model.ProductDetail;
import com.adp.interview.model.TaxDetail;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class ADPDataMapUtil<T> {

	static ObjectMapper mapper = new ObjectMapper();

	public static void saveData(Class<?> type, CommonAttribute result)
			throws JsonParseException, JsonMappingException, IOException {
		Map<String, Object> incomingSaveData = mapper.convertValue(result, new TypeReference<Map<String, Object>>() {
		});
		if (type.getSimpleName().toLowerCase().equals("productdetail")) {
			File productFile = new File(
					ADPDataMapUtil.class.getClassLoader().getResource("sample/product.json").getFile());
			Map<String, Map<String, Object>> productResult = mapper.readValue(productFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			productResult.put((String) incomingSaveData.get("productId"), incomingSaveData);
			// mapper.writeValue(productFile, productResult);
			mapper.writeValue(productFile, productResult);

		}
		if (type.getSimpleName().toLowerCase().equals("categorydetail")) {
			File categoryFile = new File(
					ADPDataMapUtil.class.getClassLoader().getResource("sample/category.json").getFile());
			Map<String, Map<String, Object>> categoryResult = mapper.readValue(categoryFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			categoryResult.put((String) incomingSaveData.get("categoryId"), incomingSaveData);
			// mapper.writeValue(categoryFile, categoryResult);
			mapper.writeValue(categoryFile, categoryResult);

		}
		if (type.getSimpleName().toLowerCase().equals("levydetail")) {
			File levyFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/levy.json").getFile());
			Map<String, Map<String, Object>> levyResult = mapper.readValue(levyFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			levyResult.put((String) incomingSaveData.get("levyId"), incomingSaveData);
			mapper.writeValue(levyFile, levyResult);

		}
		if (type.getSimpleName().toLowerCase().equals("taxdetail")) {
			File taxFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/tax.json").getFile());
			Map<String, Map<String, Object>> taxResult = mapper.readValue(taxFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			taxResult.put((String) incomingSaveData.get("taxId"), incomingSaveData);
			mapper.writeValue(taxFile, taxResult);

		}
		if (type.getSimpleName().equals("bill")) {
			File billFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/bill.json").getFile());
			Map<String, Map<String, Object>> billResult = mapper.readValue(billFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			billResult.put((String) incomingSaveData.get("billId"), incomingSaveData);
			mapper.writeValue(billFile, billResult);

		}

	}

	public static CommonAttribute findById(Class<?> inputClass, String inputId)
			throws JsonParseException, JsonMappingException, IOException {
		if (inputClass.getSimpleName().toLowerCase().equals("productdetail")) {
			File productFile = new File(
					ADPDataMapUtil.class.getClassLoader().getResource("sample/product.json").getFile());
			Map<String, Map<String, Object>> productResult = mapper.readValue(productFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			return mapper.convertValue(productResult.get(inputId), ProductDetail.class);
		}
		if (inputClass.getSimpleName().toLowerCase().equals("categorydetail")) {
			File categoryFile = new File(
					ADPDataMapUtil.class.getClassLoader().getResource("sample/category.json").getFile());
			Map<String, Map<String, Object>> categoryResult = mapper.readValue(categoryFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			return mapper.convertValue(categoryResult.get(inputId), CategoryDetail.class);
		}
		if (inputClass.getSimpleName().toLowerCase().equals("levydetail")) {
			File levyFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/levy.json").getFile());
			Map<String, Map<String, Object>> levyResult = mapper.readValue(levyFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			return mapper.convertValue(levyResult.get(inputId), LevyDetail.class);

		}
		if (inputClass.getSimpleName().toLowerCase().equals("taxdetail")) {
			File taxFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/tax.json").getFile());
			Map<String, Map<String, Object>> taxResult = mapper.readValue(taxFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			return mapper.convertValue(taxResult.get(inputId), TaxDetail.class);
		}
		if (inputClass.getSimpleName().equals("bill")) {
			File billFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/bill.json").getFile());
			Map<String, Map<String, Object>> billResult = mapper.readValue(billFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			return mapper.convertValue(billResult.get(inputId), BillDetail.class);
		}
		return null;
	}

	public static Map<String, Map<String, Object>> findAll(Class<?> inputClass)
			throws JsonParseException, JsonMappingException, IOException {
		if (inputClass.getSimpleName().toLowerCase().equals("productdetail")) {
			File productFile = new File(
					ADPDataMapUtil.class.getClassLoader().getResource("sample/product.json").getFile());
			return mapper.readValue(productFile, new TypeReference<Map<String, Map<String, Object>>>() {
			});
		}
		if (inputClass.getSimpleName().toLowerCase().equals("categorydetail")) {
			File categoryFile = new File(
					ADPDataMapUtil.class.getClassLoader().getResource("sample/category.json").getFile());
			Map<String, Map<String, Object>> categoryResult = mapper.readValue(categoryFile,
					new TypeReference<Map<String, Map<String, Object>>>() {
					});
			return mapper.convertValue(categoryResult.values(), new TypeReference<List<CategoryDetail>>() {
			});
		}
		if (inputClass.getSimpleName().toLowerCase().equals("levydetail")) {
			File levyFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/levy.json").getFile());
			return mapper.readValue(levyFile, new TypeReference<Map<String, Map<String, Object>>>() {
			});

		}
		if (inputClass.getSimpleName().toLowerCase().equals("taxdetail")) {
			File taxFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/tax.json").getFile());
			return mapper.readValue(taxFile, new TypeReference<Map<String, Map<String, Object>>>() {
			});

		}
		if (inputClass.getSimpleName().equals("bill")) {
			File billFile = new File(ADPDataMapUtil.class.getClassLoader().getResource("sample/bill.json").getFile());
			return mapper.readValue(billFile, new TypeReference<Map<String, Map<String, Object>>>() {
			});
		}
		return null;
	}

}
