package com.adp.interview.service.impl;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.adp.interview.constant.LevyStatus;
import com.adp.interview.model.LevyDetail;
import com.adp.interview.model.TaxDetail;
import com.adp.interview.repo.LevyDetailRepo;
import com.adp.interview.repo.impl.LevyDetailRepoImpl;
import com.adp.interview.service.LevyDetailService;
import com.adp.interview.util.ADPCommonUtils;
import com.adp.interview.util.ADPDataMapUtil;

public class LevyDetailServiceImpl implements LevyDetailService {

	private LevyDetailRepo<LevyDetail> levyDetailRepo;

	public LevyDetailServiceImpl() {
		levyDetailRepo = new LevyDetailRepoImpl();
	}

	@Override
	public LevyDetail createLevyDetail(LevyDetail levy) throws Exception {
		if (null != levy) {
			if (StringUtils.isEmpty(levy.getLevyId()))
				levy.setLevyId(ADPCommonUtils.generateLevyId());
			return levyDetailRepo.createLevyDetail(levy);
		}
		return null;
	}

	@Override
	public LevyDetail updateLevyDetail(LevyDetail levy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LevyDetail findByLevyId(String levyId) throws Exception {
		return levyDetailRepo.findLevyDetail(levyId);
	}

	@Override
	public boolean checkLevyActiveStatus(String levyId) throws Exception {
		LevyDetail levyDetails = levyDetailRepo.findLevyDetail(levyId);
		return LevyStatus.ACTIVE.name().equals(levyDetails.getStatus());
	}

	@Override
	public Map<String, Map<String, Object>> findAllLevy() throws Exception {
		return ADPDataMapUtil.findAll(LevyDetail.class);
	}

}
