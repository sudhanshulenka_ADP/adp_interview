package com.adp.interview.service.impl;

import java.math.BigDecimal;

import com.adp.interview.model.BillDetail;
import com.adp.interview.model.BillProduct;
import com.adp.interview.model.ProductDetail;
import com.adp.interview.repo.BillDetailsRepo;
import com.adp.interview.repo.impl.BillDetailsRepoImpl;
import com.adp.interview.service.BillDetailService;
import com.adp.interview.service.ProductDetailService;

public class BillDetailServiceImpl implements BillDetailService {

	private BillDetailsRepo billDetailsRepo;
	private ProductDetailService productDetailService;

	public BillDetailServiceImpl() {
		productDetailService = new ProductDetailServiceImpl();
		billDetailsRepo = new BillDetailsRepoImpl();
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.service.BillDetailService#processBillForProduct(com.adp.interview.model.BillProduct)
	 */
	@Override
	public void processBillForProduct(BillProduct billProduct) throws Exception {
		ProductDetail productDetails = productDetailService.findProductByProductId(billProduct.getProductId());
		BigDecimal totalPrice = productDetails.getPrice().multiply(new BigDecimal(billProduct.getOrderQuantity()));
		billProduct.setTotalPrice(billProduct.getTotalPrice().add(totalPrice));
		BigDecimal levyAmount = productDetails.getCategory().getAppliedLevy().getAmount();
		billProduct.setLevyPrice(billProduct.getLevyPrice().add(levyAmount));
		billProduct.setTaxPrice(productDetails.getCategory().getAppliedTaxType().getValue());
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.service.BillDetailService#checkOutBill(com.adp.interview.model.BillDetail, com.adp.interview.model.BillProduct)
	 */
	@Override
	public void checkOutBill(BillDetail billDetail, BillProduct billProduct) throws Exception {
		billDetail.setBilledAmount(billDetail.getBilledAmount().add(billProduct.getTotalPrice()));
		billDetail.setTotalLevyAmount(billDetail.getTotalLevyAmount().add(billProduct.getLevyPrice()));
		billDetail.setTotalTaxAmount(billDetail.getTotalTaxAmount().add(billProduct.getTaxPrice()));
	}

}
