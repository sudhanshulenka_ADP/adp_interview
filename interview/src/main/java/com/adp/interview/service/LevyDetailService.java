package com.adp.interview.service;

import java.util.Map;

import com.adp.interview.model.LevyDetail;

/**
 * 
 * @author sudhanshu
 *
 */
public interface LevyDetailService {

	LevyDetail createLevyDetail(LevyDetail leveyDetail) throws Exception;

	LevyDetail updateLevyDetail(LevyDetail levy);

	LevyDetail findByLevyId(String levyId) throws Exception;

	boolean checkLevyActiveStatus(String levyId) throws Exception;

	Map<String, Map<String, Object>> findAllLevy() throws Exception;

}
