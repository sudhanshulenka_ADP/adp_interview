package com.adp.interview.service;

import java.util.List;
import java.util.Map;

import com.adp.interview.model.TaxDetail;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public interface TaxDetailService {

	TaxDetail createTaxDetail(TaxDetail taxDetail) throws Exception;

	TaxDetail updateTaxDetail(TaxDetail taxDetail);

	TaxDetail findByTaxId(String taxId) throws Exception;

	Map<String, Map<String, Object>> findAllTax() throws Exception;

}
