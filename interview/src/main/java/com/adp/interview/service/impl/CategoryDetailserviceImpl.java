package com.adp.interview.service.impl;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.adp.interview.model.CategoryDetail;
import com.adp.interview.model.LevyDetail;
import com.adp.interview.model.TaxDetail;
import com.adp.interview.repo.CategoryDetailRepo;
import com.adp.interview.repo.impl.CategoryDetailsRepoImpl;
import com.adp.interview.service.CategoryDetailService;
import com.adp.interview.service.LevyDetailService;
import com.adp.interview.service.TaxDetailService;
import com.adp.interview.util.ADPCommonUtils;
import com.adp.interview.util.ADPDataMapUtil;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public class CategoryDetailserviceImpl implements CategoryDetailService {

	private CategoryDetailRepo<CategoryDetail> categoryDetailRepo;
	private LevyDetailService levyDetailService;
	private TaxDetailService taxDetailService;

	/**
	 * 
	 */
	public CategoryDetailserviceImpl() {
		categoryDetailRepo = new CategoryDetailsRepoImpl();
		levyDetailService = new LevyDetailServiceImpl();
		taxDetailService = new TaxDetailsServiceImpl();
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.service.CategoryDetailService#createCategory(com.adp.interview.model.CategoryDetail)
	 */
	@Override
	public CategoryDetail createCategory(CategoryDetail category) throws Exception {
		if (null != category) {
			if (StringUtils.isEmpty(category.getCategoryId()))
				category.setCategoryId(ADPCommonUtils.generateCategoryId());
			return categoryDetailRepo.createCategory(category);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.service.CategoryDetailService#applyLevyOnCategory(java.lang.String, java.lang.String)
	 */
	@Override
	public CategoryDetail applyLevyOnCategory(String categoryId, String levyId) throws Exception {
		if (!StringUtils.isEmpty(categoryId)) {
			CategoryDetail categoryDetails = categoryDetailRepo.findCategoryById(categoryId);
			LevyDetail levyDetails = levyDetailService.findByLevyId(levyId);
			categoryDetails.setAppliedLevy(levyDetails);
			return categoryDetailRepo.createCategory(categoryDetails);
		} else {
			throw new Exception("Category Id canot be null");
		}

	}

	/* (non-Javadoc)
	 * @see com.adp.interview.service.CategoryDetailService#applyTaxOnCategory(java.lang.String, java.lang.String)
	 */
	@Override
	public CategoryDetail applyTaxOnCategory(String categoryId, String taxId) throws Exception {
		if (!StringUtils.isEmpty(categoryId)) {
			CategoryDetail categoryDetails = categoryDetailRepo.findCategoryById(categoryId);
			TaxDetail taxDetails = taxDetailService.findByTaxId(taxId);
			categoryDetails.setAppliedTaxType(taxDetails);
			return categoryDetailRepo.createCategory(categoryDetails);
		} else {
			throw new Exception("Category Id canot be null");
		}
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.service.CategoryDetailService#findByCategoryId(java.lang.String)
	 */
	@Override
	public CategoryDetail findByCategoryId(String categoryId) throws Exception {
		return categoryDetailRepo.findCategoryById(categoryId);
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.service.CategoryDetailService#findAllCategory()
	 */
	@Override
	public Map<String, Map<String, Object>> findAllCategory() throws Exception {
		return ADPDataMapUtil.findAll(CategoryDetail.class);
	}

}
