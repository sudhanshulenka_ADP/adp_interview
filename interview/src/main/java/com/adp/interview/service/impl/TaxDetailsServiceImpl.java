package com.adp.interview.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.adp.interview.model.TaxDetail;
import com.adp.interview.repo.TaxDetailRepo;
import com.adp.interview.repo.impl.TaxDetailRepoImpl;
import com.adp.interview.service.TaxDetailService;
import com.adp.interview.util.ADPCommonUtils;
import com.adp.interview.util.ADPDataMapUtil;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public class TaxDetailsServiceImpl implements TaxDetailService {

	private TaxDetailRepo<TaxDetail> taxDetailRepo;

	public TaxDetailsServiceImpl() {
		taxDetailRepo = new TaxDetailRepoImpl();
	}

	@Override
	public TaxDetail createTaxDetail(TaxDetail taxDetail) throws Exception {
		if (null != taxDetail) {
			if (StringUtils.isEmpty(taxDetail.getTaxId()))
				taxDetail.setTaxId(ADPCommonUtils.generateTaxId());
			
			return taxDetailRepo.createTaxDetail(taxDetail);
		}
		return null;
	}

	@Override
	public TaxDetail updateTaxDetail(TaxDetail taxDetail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TaxDetail findByTaxId(String taxId) throws Exception {
		return (TaxDetail) ADPDataMapUtil.findById(TaxDetail.class, taxId);
	}

	@Override
	public Map<String, Map<String, Object>> findAllTax() throws Exception {
		return ADPDataMapUtil.findAll(TaxDetail.class);
	}

}
