package com.adp.interview.service;

import java.util.Map;

import com.adp.interview.model.ProductDetail;

/**
 * 
 * @author sudhanshu
 *
 */
public interface ProductDetailService {

	ProductDetail createProduct(ProductDetail product) throws Exception;

	ProductDetail updateProduct(ProductDetail product);

	ProductDetail findProductByProductId(String productId) throws Exception;

	ProductDetail findProductByProductIdAndApplyLevy(String productId, String levyId);

	Map<String, Map<String, Object>> findAllProduct() throws Exception;

}
