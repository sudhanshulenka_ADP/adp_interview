package com.adp.interview.service.impl;

import java.util.Map;

import com.adp.interview.model.CategoryDetail;
import com.adp.interview.model.LevyDetail;
import com.adp.interview.model.ProductDetail;
import com.adp.interview.repo.CategoryDetailRepo;
import com.adp.interview.repo.ProductDetailRepo;
import com.adp.interview.repo.impl.CategoryDetailsRepoImpl;
import com.adp.interview.repo.impl.ProductDetailsRepoImpl;
import com.adp.interview.service.ProductDetailService;
import com.adp.interview.util.ADPCommonUtils;
import com.adp.interview.util.ADPDataMapUtil;

/**
 * 
 * @author sudhanshu
 *
 */
public class ProductDetailServiceImpl implements ProductDetailService {

	private ProductDetailRepo<ProductDetail> productDetailsRepo;
	private CategoryDetailRepo<CategoryDetail> categoryDetailRepo;

	public ProductDetailServiceImpl() {
		productDetailsRepo = new ProductDetailsRepoImpl();
		categoryDetailRepo = new CategoryDetailsRepoImpl();
	}

	@Override
	public ProductDetail createProduct(ProductDetail product) throws Exception {
		if (null != product.getCategory()) {
			product.setProductId(ADPCommonUtils.generateProductId());
			productDetailsRepo.createProduct(product);
		} else {
			product = null;
			throw new Exception("Check With your product category");
		}

		return product;
	}

	@Override
	public ProductDetail updateProduct(ProductDetail product) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProductDetail findProductByProductId(String productId) throws Exception {
		return productDetailsRepo.findProductByProductId(productId);
	}

	@Override
	public ProductDetail findProductByProductIdAndApplyLevy(String productId, String levyId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Map<String, Object>> findAllProduct() throws Exception {
		return ADPDataMapUtil.findAll(ProductDetail.class);
	}

}
