package com.adp.interview.service;

import com.adp.interview.model.BillDetail;
import com.adp.interview.model.BillProduct;

public interface BillDetailService {

	/**
	 * @param billProduct
	 * @throws Exception
	 */
	void processBillForProduct(BillProduct billProduct) throws Exception;

	/**
	 * @param billDetail
	 * @param billProduct
	 * @throws Exception
	 */
	void checkOutBill(BillDetail billDetail, BillProduct billProduct) throws Exception;

}
