package com.adp.interview.service;

import java.util.Map;

import com.adp.interview.model.CategoryDetail;
import com.adp.interview.model.LevyDetail;
import com.adp.interview.model.TaxDetail;

/**
 * 
 * @author sudhanshu
 *
 */
public interface CategoryDetailService {

	CategoryDetail createCategory(CategoryDetail category) throws Exception;

	CategoryDetail applyLevyOnCategory(String categoryId, String levyI) throws Exception;

	CategoryDetail applyTaxOnCategory(String categoryId, String taxId) throws Exception;

	CategoryDetail findByCategoryId(String categoryId) throws Exception;

	Map<String, Map<String, Object>> findAllCategory() throws Exception;

}
