package com.adp.interview.repo.impl;

import com.adp.interview.model.BillDetail;
import com.adp.interview.model.BillProduct;
import com.adp.interview.repo.BillDetailsRepo;
import com.adp.interview.util.ADPDataMapUtil;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public class BillDetailsRepoImpl implements BillDetailsRepo<BillDetail> {

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.BillDetailsRepo#saveBill(java.lang.Object)
	 */
	@Override
	public String saveBill(BillDetail bill) throws Exception {
		ADPDataMapUtil.saveData(BillDetail.class, bill);
		return bill.getBillId();
	}


}
