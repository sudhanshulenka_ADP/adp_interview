package com.adp.interview.repo.impl;

import java.util.List;

import com.adp.interview.model.TaxDetail;
import com.adp.interview.repo.TaxDetailRepo;
import com.adp.interview.util.ADPDataMapUtil;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public class TaxDetailRepoImpl implements TaxDetailRepo<TaxDetail> {

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.TaxDetailRepo#createTaxDetail(java.lang.Object)
	 */
	@Override
	public TaxDetail createTaxDetail(TaxDetail tax) throws Exception {
		ADPDataMapUtil.saveData(TaxDetail.class, tax);
		return tax;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.TaxDetailRepo#updateTaxDetail(java.lang.Object)
	 */
	@Override
	public TaxDetail updateTaxDetail(TaxDetail tax) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.TaxDetailRepo#deleteTaxDetail(java.lang.String)
	 */
	@Override
	public TaxDetail deleteTaxDetail(String taxId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.TaxDetailRepo#findTaxDetail(java.lang.String)
	 */
	@Override
	public TaxDetail findTaxDetail(String taxId) throws Exception {
		return (TaxDetail) ADPDataMapUtil.findById(TaxDetail.class, taxId);
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.TaxDetailRepo#allTaxDetail()
	 */
	@Override
	public List<TaxDetail> allTaxDetail() {
		// TODO Auto-generated method stub
		return null;
	}

}
