package com.adp.interview.repo;

import java.util.List;

import com.adp.interview.model.CategoryDetail;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public interface CategoryDetailRepo<CategoryDetail> {

	CategoryDetail createCategory(CategoryDetail category) throws Exception;

	CategoryDetail updateCategory(CategoryDetail category);

	CategoryDetail deleteCategory(String categoryId) throws Exception;

	CategoryDetail findCategoryById(String categoryId)throws Exception;

	List<CategoryDetail> allCategory();

}
