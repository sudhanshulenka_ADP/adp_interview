package com.adp.interview.repo;

import java.util.List;

import com.adp.interview.model.ProductDetail;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public interface ProductDetailRepo<ProductDetail> {

	ProductDetail createProduct(ProductDetail product) throws Exception;

	ProductDetail updateProduct(ProductDetail product);

	ProductDetail removeProductByProductId(String productId);

	ProductDetail findProductByProductId(String productId) throws Exception;

	List<ProductDetail> findAllProduct();

}
