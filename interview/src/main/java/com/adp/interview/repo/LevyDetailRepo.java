package com.adp.interview.repo;

import java.util.List;

/**
 * 
 * @author sudhanshu.lenka
 *
 * @param <LevyDetail>
 */
public interface LevyDetailRepo<LevyDetail> {

	LevyDetail createLevyDetail(LevyDetail levy) throws Exception;

	LevyDetail updateLevyDetail(LevyDetail levy);

	LevyDetail deleteLevyDetail(String levyId);

	LevyDetail findLevyDetail(String levyId) throws Exception;

	List<LevyDetail> allLevyDetail();

}
