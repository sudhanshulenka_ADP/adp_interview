package com.adp.interview.repo.impl;

import java.util.List;

import com.adp.interview.model.LevyDetail;
import com.adp.interview.repo.LevyDetailRepo;
import com.adp.interview.util.ADPDataMapUtil;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public class LevyDetailRepoImpl implements LevyDetailRepo<LevyDetail> {

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.LevyDetailRepo#createLevyDetail(java.lang.Object)
	 */
	@Override
	public LevyDetail createLevyDetail(LevyDetail levy) throws Exception {
		ADPDataMapUtil.saveData(LevyDetail.class, levy);
		return levy;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.LevyDetailRepo#updateLevyDetail(java.lang.Object)
	 */
	@Override
	public LevyDetail updateLevyDetail(LevyDetail levy) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.LevyDetailRepo#deleteLevyDetail(java.lang.String)
	 */
	@Override
	public LevyDetail deleteLevyDetail(String levyId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.LevyDetailRepo#findLevyDetail(java.lang.String)
	 */
	@Override
	public LevyDetail findLevyDetail(String levyId) throws Exception {
		return (LevyDetail) ADPDataMapUtil.findById(LevyDetail.class, levyId);
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.LevyDetailRepo#allLevyDetail()
	 */
	@Override
	public List<LevyDetail> allLevyDetail() {
		// TODO Auto-generated method stub
		return null;
	}

}
