package com.adp.interview.repo;

import java.util.List;

/**
 * 
 * @author sudhanshu.lenka
 *
 * @param <TaxDetail>
 */
public interface TaxDetailRepo<TaxDetail> {

	TaxDetail createTaxDetail(TaxDetail tax) throws Exception;

	TaxDetail updateTaxDetail(TaxDetail tax);

	TaxDetail deleteTaxDetail(String taxId);

	TaxDetail findTaxDetail(String taxId) throws Exception;

	List<TaxDetail> allTaxDetail();

}
