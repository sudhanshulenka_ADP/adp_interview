package com.adp.interview.repo;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public interface BillDetailsRepo<BillDetail> {

	String saveBill(BillDetail bill) throws Exception;

}
