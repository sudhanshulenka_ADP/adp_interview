package com.adp.interview.repo.impl;

import java.util.List;

import com.adp.interview.model.CategoryDetail;
import com.adp.interview.repo.CategoryDetailRepo;
import com.adp.interview.util.ADPDataMapUtil;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public class CategoryDetailsRepoImpl implements CategoryDetailRepo<CategoryDetail> {

	@Override
	public CategoryDetail createCategory(CategoryDetail category) throws Exception {
		ADPDataMapUtil.saveData(CategoryDetail.class, category);
		return category;
	}

	@Override
	public CategoryDetail updateCategory(CategoryDetail category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CategoryDetail deleteCategory(String categoryId) {

		return null;
	}

	@Override
	public CategoryDetail findCategoryById(String categoryId) throws Exception {
		return (CategoryDetail) ADPDataMapUtil.findById(CategoryDetail.class, categoryId);
	}

	@Override
	public List<CategoryDetail> allCategory() {
		// TODO Auto-generated method stub
		return null;
	}

}
