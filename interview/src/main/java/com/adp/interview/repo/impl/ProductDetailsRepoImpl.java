package com.adp.interview.repo.impl;

import java.util.List;

import com.adp.interview.model.ProductDetail;
import com.adp.interview.repo.ProductDetailRepo;
import com.adp.interview.util.ADPDataMapUtil;

/**
 * 
 * @author sudhanshu.lenka
 *
 */
public class ProductDetailsRepoImpl implements ProductDetailRepo<ProductDetail> {

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.ProductDetailRepo#createProduct(java.lang.Object)
	 */
	@Override
	public ProductDetail createProduct(ProductDetail product) throws Exception {
		ADPDataMapUtil.saveData(ProductDetail.class, product);
		return product;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.ProductDetailRepo#updateProduct(java.lang.Object)
	 */
	@Override
	public ProductDetail updateProduct(ProductDetail product) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.ProductDetailRepo#removeProductByProductId(java.lang.String)
	 */
	@Override
	public ProductDetail removeProductByProductId(String productId) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.ProductDetailRepo#findProductByProductId(java.lang.String)
	 */
	@Override
	public ProductDetail findProductByProductId(String productId) throws Exception {
		return (ProductDetail) ADPDataMapUtil.findById(ProductDetail.class, productId);
	}

	/* (non-Javadoc)
	 * @see com.adp.interview.repo.ProductDetailRepo#findAllProduct()
	 */
	@Override
	public List<ProductDetail> findAllProduct() {
		// TODO Auto-generated method stub
		return null;
	}

}
