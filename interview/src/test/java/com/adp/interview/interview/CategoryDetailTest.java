package com.adp.interview.interview;

import static org.junit.Assert.assertNotNull;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.adp.interview.model.CategoryDetail;
import com.adp.interview.model.LevyDetail;
import com.adp.interview.model.TaxDetail;
import com.adp.interview.service.CategoryDetailService;
import com.adp.interview.service.LevyDetailService;
import com.adp.interview.service.TaxDetailService;
import com.adp.interview.service.impl.CategoryDetailserviceImpl;
import com.adp.interview.service.impl.LevyDetailServiceImpl;
import com.adp.interview.service.impl.TaxDetailsServiceImpl;

import junit.framework.Assert;

public class CategoryDetailTest {

	private static final Logger LOGGER = Logger.getLogger(ProductDetailTest.class.getName());

	private CategoryDetailService categoryService;
	private LevyDetailService levyDetailService;
	private TaxDetailService taxDetailService;

	@Before
	public void loadContext() {
		categoryService = new CategoryDetailserviceImpl();
		levyDetailService = new LevyDetailServiceImpl();
		taxDetailService = new TaxDetailsServiceImpl();
	}

	@Test
	public void createCategory() {
		CategoryDetail category = null;
		try {
			category = new CategoryDetail();
			category.setDisplayName("DRINKS");
			category.setName("drink");
			category.setType("cdrink");
			assertNotNull(categoryService.createCategory(category));
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(category);
		}

	}

	@Test
	public void createCategoryAndApplyLevy() {
		CategoryDetail category = null;
		try {
			category = categoryService.applyLevyOnCategory("WixK", "uKn");
			Assert.assertEquals("uKn", category.getAppliedLevy().getLevyId());
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(category);
		}

	}

	@Test
	public void createCategoryAndApplyTax() {
		CategoryDetail category = null;
		try {
			category = categoryService.applyTaxOnCategory("WixK", "ZRq");
			Assert.assertEquals("ZRq", category.getAppliedTaxType().getTaxId());
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(category);
		}

	}

	@Test
	public void createCategoryApplyTaxAndWithLevy() {
		CategoryDetail category = null;
		try {
			category = new CategoryDetail();

			category.setDisplayName("DRINKS");
			category.setName("drink");
			category.setType("cdrink");
			LevyDetail levyDetails = levyDetailService.findByLevyId("uKn");
			TaxDetail taxDetails = taxDetailService.findByTaxId("ZRq");
			category.setAppliedLevy(levyDetails);
			category.setAppliedTaxType(taxDetails);
			assertNotNull(categoryService.createCategory(category));
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(category);
		}

	}

	@Test
	public void findCategoryById() {
		CategoryDetail category = null;
		try {
			category = categoryService.findByCategoryId("WixK");
			assertNotNull(category.getCategoryId());
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(category);
		}

	}

	@Test
	public void findAllCategories() {
		Map<String, Map<String, Object>> categoryList = null;
		try {
			categoryList = categoryService.findAllCategory();
			Assert.assertTrue(categoryList.size() > 0);
		} catch (Exception ex) {
			Assert.assertTrue(categoryList.size() > 0);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}
}
