package com.adp.interview.interview;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.adp.interview.model.BillDetail;
import com.adp.interview.model.BillProduct;
import com.adp.interview.service.BillDetailService;
import com.adp.interview.service.ProductDetailService;
import com.adp.interview.service.impl.BillDetailServiceImpl;
import com.adp.interview.service.impl.ProductDetailServiceImpl;

public class BillDetailServiceTest {

	private static final Logger LOGGER = Logger.getLogger(BillDetailServiceTest.class.getName());

	private ProductDetailService productDetailService;
	private BillDetailService billDetailService;

	@Before
	public void loadContext() {

		productDetailService = new ProductDetailServiceImpl();
		billDetailService = new BillDetailServiceImpl();
	}

	@Test
	public void createBillForProduct() {
		BillDetail bill = new BillDetail();
		try {
			BillProduct billProduct = new BillProduct();
			billProduct.setProductId("AlyNTI");
			billProduct.setOrderQuantity(10d);
			billDetailService.processBillForProduct(billProduct);
			billDetailService.checkOutBill(bill, billProduct);
			assertNotNull(bill);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNull(bill.getBillId());
		}

	}

	@Test
	public void scannProduct() {
		BillDetail bill = new BillDetail();
		try {
			BillProduct billProduct = new BillProduct();
			billProduct.setProductId("AlyNTI");
			billProduct.setOrderQuantity(10d);
			billDetailService.processBillForProduct(billProduct);
			billDetailService.checkOutBill(bill, billProduct);
			assertNotNull(bill);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNull(bill.getBillId());
		}

	}

	@Test
	public void calculateTotalTaxOnProduct() {
		BillDetail bill = new BillDetail();
		try {
			BillProduct billProduct = new BillProduct();
			billProduct.setProductId("AlyNTI");
			billProduct.setOrderQuantity(10d);
			billDetailService.processBillForProduct(billProduct);
			billDetailService.checkOutBill(bill, billProduct);
			assertNotNull(bill);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNull(bill.getBillId());
		}

	}

	@Test
	public void calculateTotalLevyOnProduct() {
		BillDetail bill = new BillDetail();
		try {
			BillProduct billProduct = new BillProduct();
			billProduct.setProductId("AlyNTI");
			billProduct.setOrderQuantity(10d);
			billDetailService.processBillForProduct(billProduct);
			billDetailService.checkOutBill(bill, billProduct);
			assertNotNull(bill);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNull(bill.getBillId());
		}

	}

	@Test
	public void calculateTotalTaxOnBill() {
		BillDetail bill = new BillDetail();
		try {
			BillProduct billProduct = new BillProduct();
			billProduct.setProductId("AlyNTI");
			billProduct.setOrderQuantity(10d);
			billDetailService.processBillForProduct(billProduct);
			billDetailService.checkOutBill(bill, billProduct);
			assertNotNull(bill);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNull(bill.getBillId());
		}

	}

	@Test
	public void calculateTotalLevyOnBill() {
		BillDetail bill = new BillDetail();
		try {
			BillProduct billProduct = new BillProduct();
			billProduct.setProductId("AlyNTI");
			billProduct.setOrderQuantity(10d);
			billDetailService.processBillForProduct(billProduct);
			billDetailService.checkOutBill(bill, billProduct);
			assertNotNull(bill);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNull(bill.getBillId());
		}

	}

}
