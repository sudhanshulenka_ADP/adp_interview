package com.adp.interview.interview;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BillDetailServiceTest.class, ProductDetailTest.class, TaxDetailTest.class, LevyDetailTest.class,
		CategoryDetailTest.class })
public class InterviewApplicationTests {

	@Test
	public void contextLoads() {
	}

}
