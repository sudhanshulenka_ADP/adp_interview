package com.adp.interview.interview;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.adp.interview.model.TaxDetail;
import com.adp.interview.service.TaxDetailService;
import com.adp.interview.service.impl.TaxDetailsServiceImpl;

import junit.framework.Assert;

public class TaxDetailTest {

	private static final Logger LOGGER = Logger.getLogger(TaxDetailTest.class.getName());
	private TaxDetailService taxDetailService;

	@Before
	public void loadContext() {
		taxDetailService = new TaxDetailsServiceImpl();
	}

	@Test
	public void createTax() {
		TaxDetail tax = null;
		try {
			tax = new TaxDetail();
			tax.setDisplayName("TAX1");
			tax.setName("tax1");
			tax.setType("CST");
			tax.setValue(new BigDecimal(12.02));
			tax.setTaxId("ZRq");
			assertNotNull(taxDetailService.createTaxDetail(tax));
		} catch (Exception ex) {
			assertNotNull(tax.getTaxId());
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

	@Test
	public void getTaxByTaxId() {
		TaxDetail tax = null;
		try {
			tax = taxDetailService.findByTaxId("ZRq");
			Assert.assertEquals("ZRq", tax.getTaxId());
		} catch (Exception ex) {
			assertNotNull(tax);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}

	@Test
	public void validateTaxAmount() {
		TaxDetail tax = null;
		try {
			tax = taxDetailService.findByTaxId("ZRq");
			Assert.assertEquals(new BigDecimal(10.02).intValue(), tax.getValue().intValue());
		} catch (Exception ex) {
			assertNotNull(tax);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

	@Test
	public void validateTaxType() {
		TaxDetail tax = null;
		try {
			tax = taxDetailService.findByTaxId("ZRq");
			Assert.assertEquals("GST", tax.getType());
		} catch (Exception ex) {
			assertNotNull(tax);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

	@Test
	public void findAllTax() {
		Map<String, Map<String, Object>> taxList = null;
		try {
			taxList = taxDetailService.findAllTax();
			Assert.assertTrue(taxList.size() > 0);
		} catch (Exception ex) {
			Assert.assertTrue(taxList.size() > 0);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

}
