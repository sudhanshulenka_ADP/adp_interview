package com.adp.interview.interview;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.adp.interview.constant.MeasurementUnit;
import com.adp.interview.model.ProductDetail;
import com.adp.interview.service.CategoryDetailService;
import com.adp.interview.service.ProductDetailService;
import com.adp.interview.service.impl.CategoryDetailserviceImpl;
import com.adp.interview.service.impl.ProductDetailServiceImpl;

import junit.framework.Assert;

public class ProductDetailTest {

	private static final Logger LOGGER = Logger.getLogger(ProductDetailTest.class.getName());

	private ProductDetailService productDetailService;

	private CategoryDetailService categoryService;

	@Before
	public void loadContext() {
		productDetailService = new ProductDetailServiceImpl();
		categoryService = new CategoryDetailserviceImpl();
	}

	@Test
	public void createProduct() {
		ProductDetail product = null;
		try {
			product = new ProductDetail();
			product.setAvilableQuantity(10.00);
			product.setDisplayName("DEMO Product Solid");
			product.setName("demoproduct");
			product.setUnit(MeasurementUnit.KG.toString());
			product.setPrice(new BigDecimal(10.45));
			assertNotNull(productDetailService.createProduct(product));
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(product);
		}

	}

	@Test
	public void createProductWithCategory() {
		ProductDetail product = null;
		try {
			product = new ProductDetail();
			product.setAvilableQuantity(10.00);
			product.setDisplayName("DEMO Product Solid");
			product.setName("demoproduct");
			product.setUnit(MeasurementUnit.KG.toString());
			product.setPrice(new BigDecimal(10.45));
			product.setCategory(categoryService.findByCategoryId("WixK"));
			product = productDetailService.createProduct(product);
			Assert.assertEquals("WixK", product.getCategory().getCategoryId());
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(product);
		}

	}

	@Test
	public void findProductByProductId() {
		ProductDetail product = null;
		try {
			product = productDetailService.findProductByProductId("AlyNTI");
			Assert.assertEquals("AlyNTI", product.getProductId());
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(product);
		}

	}

	@Test
	public void findAllProduct() {
		Map<String, Map<String, Object>> productList = null;
		try {
			productList = productDetailService.findAllProduct();
			Assert.assertTrue(productList.size() > 0);
		} catch (Exception ex) {
			Assert.assertTrue(productList.size() > 0);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

}
