package com.adp.interview.interview;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.adp.interview.constant.LevyStatus;
import com.adp.interview.constant.LevyType;
import com.adp.interview.model.LevyDetail;
import com.adp.interview.model.TaxDetail;
import com.adp.interview.service.LevyDetailService;
import com.adp.interview.service.impl.LevyDetailServiceImpl;
import com.adp.interview.util.ADPCommonUtils;

import junit.framework.Assert;

public class LevyDetailTest {
	private static final Logger LOGGER = Logger.getLogger(LevyDetailTest.class.getName());

	private LevyDetailService levyDetailService;

	@Before
	public void loadContext() {
		levyDetailService = new LevyDetailServiceImpl();
	}

	@Test
	public void createLevy() {
		LevyDetail levy = null;
		try {
			levy = new LevyDetail();
			levy.setAmount(new BigDecimal(10.00));
			levy.setDisplayName("SUMMER OFFER");
			levy.setName("summeroffer");
			levy.setStatus(LevyStatus.ACTIVE.toString());
			levy.setType(LevyType.PERCENTAGE.toString());
			assertNotNull(levyDetailService.createLevyDetail(levy));
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			assertNotNull(levy);
		}

	}

	@Test
	public void getLevyById() {
		LevyDetail levy = null;
		try {
			levy = levyDetailService.findByLevyId("uKn");
			Assert.assertEquals("uKn", levy.getLevyId());
		} catch (Exception ex) {
			assertNotNull(levy);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}

	@Test
	public void checkLevyStatus() {
		boolean isLevyActive = false;
		try {
			isLevyActive = levyDetailService.checkLevyActiveStatus("uKn");
			Assert.assertTrue(isLevyActive);
		} catch (Exception ex) {
			Assert.assertTrue(isLevyActive);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

	@Test
	public void checkLevyAmount() {
		LevyDetail levy = null;
		try {
			levy = levyDetailService.findByLevyId("uKn");
			Assert.assertEquals(new BigDecimal(10.00).intValue(), levy.getAmount().intValue());
		} catch (Exception ex) {
			assertNotNull(levy);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

	@Test
	public void findAllLevy() {
		Map<String, Map<String, Object>> levyList = null;
		try {
			levyList = levyDetailService.findAllLevy();
			Assert.assertTrue(levyList.size() > 0);
		} catch (Exception ex) {
			Assert.assertTrue(levyList.size() > 0);
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}

	}

}
